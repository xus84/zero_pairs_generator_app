let people
 document.body.style.backgroundImage = "url('src/img/SF2.webp')"
document.body.style.backgroundRepeat = "no-repeat"
document.body.style.backgroundSize = "cover" 


const peopleAsJson = localStorage.getItem('people')
if (peopleAsJson === null) {
  people = []
} else {
  people = JSON.parse(peopleAsJson)
}

document.querySelector('#new-person').addEventListener('keyup', (event) => {
  const isEnterPressed = (event.key === 'Enter')
  const newPerson = event.target.value

  if (newPerson && isEnterPressed) {
    people.push(newPerson)
    localStorage.setItem('people', JSON.stringify(people))
    drawPeople()
    event.target.value = ''
  }
})

const removePersonByName = (name) => {
  people = people.filter(person => person !== name)
  localStorage.setItem('people', JSON.stringify(people))
  drawPeople()
}

const drawPeople = () => {
  const peopleList = document.querySelector("#people")
  peopleList.innerHTML = ''
  people.forEach((person) => {
    const personAsHtml = `<li><span>${person}</span><span class="icon" onclick="removePersonByName('${person}')" ></span></li>`

    peopleList.innerHTML += personAsHtml
  })
}
document.body.onload = drawPeople

document.querySelector('#arcade-button').addEventListener('click', () => {
  const pairs = FirstPairs.generateRandomListWith(people)

  const pairList = document.querySelector('#pairs')
  pairList.innerHTML = ''
  pairs.forEach((pair) => {
    const pairAsHtml = `
      <ul>
          <li style="color:red; font-size: 1.5rem;">Driver: ${pair.driver}</li>
          <li style="color:blue; font-size: 1.5rem;">Navigator: ${pair.navigator}</li>
      </ul>
    `

    pairList.innerHTML += pairAsHtml
  });
})
