const People = class {
        constructor(list) {
          this.list = [...list]
        }
      
        extract() {
          return this.list.shift()
        }
      
        isThereAnyone() {
          return this.list.length > 0
        }
      }
      