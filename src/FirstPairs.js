
const FirstPairs = class {
        static generateRandomListWith(list) {
          const randomList = this._randomize(list)
      
          const pairs = this._generateListWith(randomList)
      
          return pairs
        }
      
        static _generateListWith(list) {
          const people = new People(list)
          const pairs = []
      
          while(people.isThereAnyone()) {
            const driver = people.extract()
            const navigator = people.extract() || driver
      
            const pair = { driver, navigator }
            pairs.push(pair)
          }
      
          return pairs
        }
      
        static _randomize(list) {
          const clonedList = [...list]
      
          const randomList = clonedList.sort((_, __) => {
            const order = [-1, 1]
            const randomNumber = Math.random() * order.length
            const index = Math.floor(randomNumber)
      
            return order[index]
          })
      
          return randomList
        }
      }
      